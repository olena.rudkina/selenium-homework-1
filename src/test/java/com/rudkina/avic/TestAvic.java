package com.rudkina.avic;

import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("Test avic.ua")
public class TestAvic {
    static WebDriver driver;

    @BeforeAll
    public static void setup() {
        System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver");
        driver = new FirefoxDriver();
        System.out.println("start tests");
    }

    @BeforeEach
    public void openChrome() {
        driver.get("https://avic.ua/ua");
    }

    @AfterEach
    public void waitTwoSec() {
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
    }

    @AfterAll
    public static void andTest() {
        driver.quit();
        System.out.println("finished");
    }

    @Test
    @DisplayName("Title should contains Avic")
    void titleShouldContainAvic() {
        assertTrue(driver.getTitle().contains("AVIC"));
    }

    @Test
    @DisplayName("Page should contains phone number")
    void pageShouldContainsPhoneNumber() {
        driver.findElement(By.xpath("//*[@id=\'mm-0\']/header/div[1]/div/div/div[3]/ul/li[3]/a")).click();
        String result = driver.findElement(By.cssSelector(".phone-item > a:nth-child(2)")).getText();
        assertEquals("0-800-307-900", result);
    }

    @Test
    @DisplayName("Urls should be the same")
    void urlShouldBeTheSame() {
        assertEquals("https://avic.ua/ua", driver.getCurrentUrl());
    }
}

